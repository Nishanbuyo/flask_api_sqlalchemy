from flask import jsonify, request, Flask, abort, make_response
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://tnahsoe:tnahsoe@localhost/testapi'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'SECRET_KEY'
db = SQLAlchemy(app)


class Posts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer, nullable=False)
    title = db.Column(db.String(120), nullable=False)
    body = db.Column(db.String(500), nullable=False)

    def __init__(self, title, body, userId):
        self.title = title
        self.body = body
        self.userId = userId


@app.route('/', methods=["GET"])
def home():
    return "Hello World"


@app.route('/posts', methods=["GET"])
def show_posts():
    output = []
    posts = Posts.query.all()
    for post in posts:
        cur_post = {}
        cur_post['userId'] = post.userId
        cur_post['title'] = post.title
        cur_post['body'] = post.body
        output.append(cur_post)
    return jsonify(output)


@app.route('/post/<int:post_id>', methods=["GET"])
def show_post(post_id):
    post = Posts.query.filter_by(id=post_id).first()
    if not post:
        return jsonify({"message": "post not found"}), 404
    output = {}
    output['userId'] = post.userId
    output['title'] = post.title
    output['body'] = post.body
    return jsonify(output)


@app.route('/add_post', methods=["POST"])
def add_post():
    data = request.get_json()
    if request.method == "POST":
        post = Posts(userId=data['userId'],
                     title=data['title'], body=data['body'])
        db.session.add(post)
        db.session.commit()
        return jsonify(data)


@app.route('/update_post/<int:post_id>', methods=["PUT"])
def update_post(post_id):
    data = request.get_json()
    if request.method == "PUT":
        post = Posts.query.filter_by(id=post_id).first()
        post.userId = data['userId']
        post.title = data['title']
        post.body = data['body']
        cur_post = {}
        cur_post['userId'] = post.userId
        cur_post['title'] = post.title
        cur_post['body'] = post.body
        db.session.commit()
        return jsonify(cur_post)



@app.route('/delete/<int:post_id>', methods=["DELETE"])
def delete_post(post_id):
    post = Posts.query.filter_by(id=post_id).first()
    db.session.delete(post)
    db.session.commit()
    return jsonify(post, {"message": "deleted succesfully"})


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)
